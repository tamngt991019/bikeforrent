﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BikeForRent.Controllers.Models
{
    public class CreateRoleRequest
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public CreateRoleRequest(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
