﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class User
    {
        public User()
        {
            BikeOwnerLocations = new HashSet<BikeOwnerLocation>();
            Bikes = new HashSet<Bike>();
            Bookings = new HashSet<Booking>();
            UserRoles = new HashSet<UserRole>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string IdentityNo { get; set; }
        public DateTime? DateCreated { get; set; }
        public byte[] Image { get; set; }
        public string Status { get; set; }

        public virtual ICollection<BikeOwnerLocation> BikeOwnerLocations { get; set; }
        public virtual ICollection<Bike> Bikes { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
