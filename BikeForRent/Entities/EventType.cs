﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class EventType
    {
        public EventType()
        {
            BookingEvents = new HashSet<BookingEvent>();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BookingEvent> BookingEvents { get; set; }
    }
}
