﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class LocationDetail
    {
        public LocationDetail()
        {
            BikeOwnerLocations = new HashSet<BikeOwnerLocation>();
            BookingLocationGetBikeNavigations = new HashSet<Booking>();
            BookingLocationReturnBikeNavigations = new HashSet<Booking>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string AreaId { get; set; }
        public long? LocationTypeId { get; set; }
        public string LocationId { get; set; }

        public virtual Area Area { get; set; }
        public virtual Location Location { get; set; }
        public virtual LocationType LocationType { get; set; }
        public virtual ICollection<BikeOwnerLocation> BikeOwnerLocations { get; set; }
        public virtual ICollection<Booking> BookingLocationGetBikeNavigations { get; set; }
        public virtual ICollection<Booking> BookingLocationReturnBikeNavigations { get; set; }
    }
}
