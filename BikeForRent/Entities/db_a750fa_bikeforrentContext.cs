﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class db_a750fa_bikeforrentContext : DbContext
    {
        public db_a750fa_bikeforrentContext()
        {
        }

        public db_a750fa_bikeforrentContext(DbContextOptions<db_a750fa_bikeforrentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<Bike> Bikes { get; set; }
        public virtual DbSet<BikeBrand> BikeBrands { get; set; }
        public virtual DbSet<BikeOwnerLocation> BikeOwnerLocations { get; set; }
        public virtual DbSet<BikeType> BikeTypes { get; set; }
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<BookingEvent> BookingEvents { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<EventType> EventTypes { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationDetail> LocationDetails { get; set; }
        public virtual DbSet<LocationType> LocationTypes { get; set; }
        public virtual DbSet<PayPackage> PayPackages { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=SQL5097.site4now.net;Initial Catalog=db_a750fa_bikeforrent;User Id=db_a750fa_bikeforrent_admin;Password=SwdB4rDb1");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Area>(entity =>
            {
                entity.ToTable("Area");

                entity.Property(e => e.Id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CityId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Areas)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK__Area__CityId__37A5467C");
            });

            modelBuilder.Entity<Bike>(entity =>
            {
                entity.ToTable("Bike");

                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BrandId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Color)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Image).HasColumnType("image");

                entity.Property(e => e.LicensePlates)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TypeId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.Bikes)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK__Bike__BrandId__31EC6D26");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Bikes)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK__Bike__TypeId__32E0915F");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.Bikes)
                    .HasForeignKey(d => d.Username)
                    .HasConstraintName("FK__Bike__Username__30F848ED");
            });

            modelBuilder.Entity<BikeBrand>(entity =>
            {
                entity.ToTable("BikeBrand");

                entity.Property(e => e.Id)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BikeOwnerLocation>(entity =>
            {
                entity.ToTable("BikeOwnerLocation");

                entity.Property(e => e.DateActive).HasColumnType("datetime");

                entity.Property(e => e.LocationDetailId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LocationDetail)
                    .WithMany(p => p.BikeOwnerLocations)
                    .HasForeignKey(d => d.LocationDetailId)
                    .HasConstraintName("FK__BikeOwner__Locat__4316F928");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.BikeOwnerLocations)
                    .HasForeignKey(d => d.Username)
                    .HasConstraintName("FK__BikeOwner__Usern__440B1D61");
            });

            modelBuilder.Entity<BikeType>(entity =>
            {
                entity.ToTable("BikeType");

                entity.Property(e => e.Id)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Booking>(entity =>
            {
                entity.ToTable("Booking");

                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BikeId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BikeRating).HasColumnName("bike_rating");

                entity.Property(e => e.BikeReport)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("bike_report");

                entity.Property(e => e.CustomerRating).HasColumnName("customer_rating");

                entity.Property(e => e.CustomerReport)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("customer_report");

                entity.Property(e => e.DateBegin)
                    .HasColumnType("datetime")
                    .HasColumnName("date_begin");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateEnd)
                    .HasColumnType("datetime")
                    .HasColumnName("date_end");

                entity.Property(e => e.LocationGetBike)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LocationReturnBike)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerReport)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("owner_report");

                entity.Property(e => e.PatPackageId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("status");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Bike)
                    .WithMany(p => p.Bookings)
                    .HasForeignKey(d => d.BikeId)
                    .HasConstraintName("FK__Booking__BikeId__4BAC3F29");

                entity.HasOne(d => d.LocationGetBikeNavigation)
                    .WithMany(p => p.BookingLocationGetBikeNavigations)
                    .HasForeignKey(d => d.LocationGetBike)
                    .HasConstraintName("FK__Booking__Locatio__4D94879B");

                entity.HasOne(d => d.LocationReturnBikeNavigation)
                    .WithMany(p => p.BookingLocationReturnBikeNavigations)
                    .HasForeignKey(d => d.LocationReturnBike)
                    .HasConstraintName("FK__Booking__Locatio__4E88ABD4");

                entity.HasOne(d => d.PatPackage)
                    .WithMany(p => p.Bookings)
                    .HasForeignKey(d => d.PatPackageId)
                    .HasConstraintName("FK__Booking__PatPack__4CA06362");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.Bookings)
                    .HasForeignKey(d => d.Username)
                    .HasConstraintName("FK__Booking__Usernam__4AB81AF0");
            });

            modelBuilder.Entity<BookingEvent>(entity =>
            {
                entity.ToTable("BookingEvent");

                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BookingId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateEnd).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EventTypeId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LocationId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.BookingEvents)
                    .HasForeignKey(d => d.BookingId)
                    .HasConstraintName("FK__BookingEv__Booki__5441852A");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.BookingEvents)
                    .HasForeignKey(d => d.EventTypeId)
                    .HasConstraintName("FK__BookingEv__Event__5535A963");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.BookingEvents)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK__BookingEv__Locat__5629CD9C");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("City");

                entity.Property(e => e.Id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EventType>(entity =>
            {
                entity.ToTable("EventType");

                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("Location");

                entity.Property(e => e.Id)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocationDetail>(entity =>
            {
                entity.ToTable("LocationDetail");

                entity.Property(e => e.Id)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AreaId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LocationId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.LocationDetails)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK__LocationD__AreaI__3E52440B");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LocationDetails)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK__LocationD__Locat__403A8C7D");

                entity.HasOne(d => d.LocationType)
                    .WithMany(p => p.LocationDetails)
                    .HasForeignKey(d => d.LocationTypeId)
                    .HasConstraintName("FK__LocationD__Locat__3F466844");
            });

            modelBuilder.Entity<LocationType>(entity =>
            {
                entity.ToTable("LocationType");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PayPackage>(entity =>
            {
                entity.ToTable("PayPackage");

                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BikeTypeId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.BikeType)
                    .WithMany(p => p.PayPackages)
                    .HasForeignKey(d => d.BikeTypeId)
                    .HasConstraintName("FK__PayPackag__BikeT__46E78A0C");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.ToTable("Payment");

                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BookingId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentTypeId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.BookingId)
                    .HasConstraintName("FK__Payment__Booking__5AEE82B9");

                entity.HasOne(d => d.PaymentType)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.PaymentTypeId)
                    .HasConstraintName("FK__Payment__Payment__5BE2A6F2");
            });

            modelBuilder.Entity<PaymentType>(entity =>
            {
                entity.ToTable("PaymentType");

                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.Id)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Username)
                    .HasName("PK__User__536C85E57CD2CAED");

                entity.ToTable("User");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IdentityNo)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Image).HasColumnType("image");

                entity.Property(e => e.Password)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNo)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.Username, e.RoleId })
                    .HasName("PK__UserRole__EBC32904A7659945");

                entity.ToTable("UserRole");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserRole__RoleId__2A4B4B5E");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserRole__Userna__29572725");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
