﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class BikeBrand
    {
        public BikeBrand()
        {
            Bikes = new HashSet<Bike>();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Bike> Bikes { get; set; }
    }
}
