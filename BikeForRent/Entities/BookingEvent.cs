﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class BookingEvent
    {
        public string Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string BookingId { get; set; }
        public string Description { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime? DateEnd { get; set; }
        public string EventTypeId { get; set; }
        public string LocationId { get; set; }

        public virtual Booking Booking { get; set; }
        public virtual EventType EventType { get; set; }
        public virtual Location Location { get; set; }
    }
}
