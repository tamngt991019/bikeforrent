﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class Area
    {
        public Area()
        {
            LocationDetails = new HashSet<LocationDetail>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string CityId { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<LocationDetail> LocationDetails { get; set; }
    }
}
