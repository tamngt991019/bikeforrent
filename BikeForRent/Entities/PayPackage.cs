﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class PayPackage
    {
        public PayPackage()
        {
            Bookings = new HashSet<Booking>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? FormulaType { get; set; }
        public string BikeTypeId { get; set; }

        public virtual BikeType BikeType { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
