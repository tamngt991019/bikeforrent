﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class Payment
    {
        public string Id { get; set; }
        public string BookingId { get; set; }
        public double? TotalPrice { get; set; }
        public string PaymentTypeId { get; set; }

        public virtual Booking Booking { get; set; }
        public virtual PaymentType PaymentType { get; set; }
    }
}
