﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class LocationType
    {
        public LocationType()
        {
            LocationDetails = new HashSet<LocationDetail>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<LocationDetail> LocationDetails { get; set; }
    }
}
