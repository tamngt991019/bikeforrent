﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class Location
    {
        public Location()
        {
            BookingEvents = new HashSet<BookingEvent>();
            LocationDetails = new HashSet<LocationDetail>();
        }

        public string Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public virtual ICollection<BookingEvent> BookingEvents { get; set; }
        public virtual ICollection<LocationDetail> LocationDetails { get; set; }
    }
}
