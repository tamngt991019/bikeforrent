﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class Booking
    {
        public Booking()
        {
            BookingEvents = new HashSet<BookingEvent>();
            Payments = new HashSet<Payment>();
        }

        public string Id { get; set; }
        public string Username { get; set; }
        public string BikeId { get; set; }
        public string PatPackageId { get; set; }
        public DateTime DateCreated { get; set; }
        public string LocationGetBike { get; set; }
        public string LocationReturnBike { get; set; }
        public DateTime? DateBegin { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? CustomerRating { get; set; }
        public string CustomerReport { get; set; }
        public string OwnerReport { get; set; }
        public int? BikeRating { get; set; }
        public string BikeReport { get; set; }
        public string Status { get; set; }

        public virtual Bike Bike { get; set; }
        public virtual LocationDetail LocationGetBikeNavigation { get; set; }
        public virtual LocationDetail LocationReturnBikeNavigation { get; set; }
        public virtual PayPackage PatPackage { get; set; }
        public virtual User UsernameNavigation { get; set; }
        public virtual ICollection<BookingEvent> BookingEvents { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
