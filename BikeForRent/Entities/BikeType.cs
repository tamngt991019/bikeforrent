﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class BikeType
    {
        public BikeType()
        {
            Bikes = new HashSet<Bike>();
            PayPackages = new HashSet<PayPackage>();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Bike> Bikes { get; set; }
        public virtual ICollection<PayPackage> PayPackages { get; set; }
    }
}
