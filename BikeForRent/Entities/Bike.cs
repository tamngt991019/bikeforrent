﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class Bike
    {
        public Bike()
        {
            Bookings = new HashSet<Booking>();
        }

        public string Id { get; set; }
        public string Username { get; set; }
        public string BrandId { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string LicensePlates { get; set; }
        public string TypeId { get; set; }
        public string Status { get; set; }
        public byte[] Image { get; set; }

        public virtual BikeBrand Brand { get; set; }
        public virtual BikeType Type { get; set; }
        public virtual User UsernameNavigation { get; set; }
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
