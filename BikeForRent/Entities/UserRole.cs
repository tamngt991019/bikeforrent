﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class UserRole
    {
        public string Username { get; set; }
        public string RoleId { get; set; }
        public string Status { get; set; }

        public virtual Role Role { get; set; }
        public virtual User UsernameNavigation { get; set; }
    }
}
