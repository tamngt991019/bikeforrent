﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BikeForRent.Entities
{
    public partial class BikeOwnerLocation
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string LocationDetailId { get; set; }
        public DateTime? DateActive { get; set; }

        public virtual LocationDetail LocationDetail { get; set; }
        public virtual User UsernameNavigation { get; set; }
    }
}
